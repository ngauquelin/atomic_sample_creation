# Atomic Sample Creation
This is a collection of examples for creating atomic structures with [atomsk](https://atomsk.univ-lille.fr/index.php) and [supercell](https://orex.github.io/supercell/). These two CLI-programs are very useful for atomic model creation. Atomsk allows the creation and modification of atomic models in various ways and supports a large number of file formats, including cif. _[1]_ Supercell allows to create structure configurations for disordered crystals _[2]_ (Occupation probabilities result in a number of possible configurations for finite crystals). Refer to the corresponding documentations to learn about the commands. Here are some example codes that illustrate common usages.
This repository also includes a container (you can think of it as a small virtual machine, living inside the repository) that can run atomsk and supercell commands. An extra command called `align_z` can be used in the docker container of this repository, allowing to read a cif file and aligning any given zone axis with the z-axis of the cartesian system. When changing and commiting the file "input.sh" a pipeline running the commands specified in the "input.sh" file starts automatically.  Under the tab "CI/CD" in the sidebar in the "[Jobs](https://gitlab.com/tfriedrich/atomic_sample_creation/-/jobs)" Menu the log of this pipeline and the output files can be found. If you want to use this container and pipeline, please fork the project into your own GitLab Account as outlined [here](https://gitlab.com/tfriedrich/atomic_sample_creation/-/blob/master/Fork_Project.md).

## Create atomic structure from scratch
```
#!/bin/bash

atomsk --create bcc 2.85 Fe Fe.xsf
```
![Result](Figures/Fe.png)

*Fe unit cell in [001] Zone axis orientation*

## Create atomic structure and transform it into a given orientation
```
#!/bin/bash

atomsk --create bcc 2.85 Fe Fe.xsf                                          # create structure with lattice vectors aligned to cartesian axis
atomsk Fe.xsf -orient [100] [010] [001] [121] [-101] [1-11] Fe_orient.xsf   # change orientation from [100] [010] [001] to [121] [-101] [1-11]
atomsk Fe_orient.xsf -duplicate 2 2 2 xyz                                   # Convert to xyz coordinates
```
![Result](Figures/Fe_orient.png)

*Fe unit cell in [111] Zone axis orientation*

## Read any cif file and align any zone axis with the cartesian z
In this example a function align_z is used. Align_z is bash script, using atomsk and octave functions to read a cif file, align the crystals given zone axis with the z-axis of the cartesian system and to calculate a orthorhombic representation of the structure. The syntax is:
`align_z input_file [zone_axis] output_file`
This procedure requieres saving an intermediate structure file.
```
#!/bin/bash

align_z cifs/VO_P21.cif [-201] temp.atsk
# align_z input_file [zone_axis] output_file
atomsk temp.atsk \
-duplicate 10 10 10  \
-cut above 25 X -cut below 0 X  \
-cut above 25 Y -cut below 0 Y  \
-cut above 25 Z -cut below 0 Z  \
VO_P21_201.xyz
rm temp.atsk
```
![Result](Figures/VO2.png)

*VO2 crystal in [-201] Zone axis orientation*

## Create supercell configurations from disordered crystal
#### Example 1 NaYF4
```
#!/bin/bash

supercell -i cifs/NaYF4.cif -s 1x1x2 -n f2 -v 2 -o NaYF4sc -m                               # Create two possible configurations
atomsk NaYF4sc_if0_w2.cif -duplicate 12 12 3 sc1.cif                                        # Expand the models
atomsk NaYF4sc_if1_w2.cif -duplicate 12 12 3 sc2.cif
atomsk sc1.cif -cut below 10 x -cut above 40 x -cut below 10 y -cut above 40 y final1.xyz   # Crop the model into squares and export as .xyz
atomsk sc2.cif -cut below 10 x -cut above 40 x -cut below 10 y -cut above 40 y final2.xyz   # Crop the model into squares and export as .xyz
```
| Structure configuration 1 | Structure configuration 2|
| ------ | ------ |
| ![Disordered structure](Figures/NaYF4_Hexagonal_SpaceGroupNo174.png) *Disordered unit cell* | ![Disordered structure](Figures/NaYF4_Hexagonal_SpaceGroupNo174.png) *Disordered unit cell*|
| ![Supercell 1](Figures/NaYF4_174_222sc_ir0_w2.png) *1x1x2 unit cells of configuration 1*|![Supercell 1](Figures/NaYF4_174_222sc_ir1_w2.png)*1x1x2 unit cells of configuration 2*|
| ![Supercell 1](Figures/final1.png) *Final structure of configuration 1*| ![Supercell 2](Figures/final2.png) *Final structure of configuration 2*| 

#### Example 2 NiMn2O4
In this example, supercell outputs only one random configuration. The bash command `$(ls | grep NiMn2O4sc)` searches the created filename and passes it to the atomsk command as input. The rest of the process is actually a single atomsk command, thus omitting the output of intermediate results. Note that for such a large and disordered unit cell, adding another unit cell (e.g. 1x1x2) would result in a huge number of possible configurations (~2e+10). This would take a serious amount of time. Therefore it is usually better to create small cells and replicate them. This introduces short range order. You'll have to decide wether that is acceptable for your case.
```
#!/bin/bash

supercell -i cifs/NiMn2O4.cif -s 1x1x1 -n r1 -v 2 -o NiMn2O4sc -m
atomsk $(ls | grep NiMn2O4sc) -duplicate 5 5 5  \
	-orient [100] [010] [001] [121] [-101] [1-11]  \
	-center com  \
	-cut below 0.35*box x -cut above 0.65*box x  \
	-cut below 0.35*box y -cut above 0.65*box y  \
	-cut below 0.35*box z -cut above 0.65*box z  \
	NiMn2O4sc111.xyz
```
| Disordered unit cell | Created Model|
| ------ | ------ |
|![Disordered structure](Figures/NiMn2O4.png) | ![Result](Figures/sc111.png) |

## Create a crystal with dislocation
```
#!/bin/bash

atomsk --create fcc 3.9242 Pt orient [110] [-111] [1-12] Pt_uc.xsf
atomsk Pt_uc.xsf -duplicate 60 20 6 Pt_supercell.xsf
atomsk Pt_supercell.xsf -dislocation 0.501*box 0.501*box edge Z Y 2.774828 0.38 Pt_supercell_edge.xyz
atomsk Pt_supercell_edge.xyz -cut below 0.4*box x -cut above 0.6*box x -cut below 0.4*box y -cut above 0.6*box y Pt_edge_cut.xyz
```
![Result](Figures/Pt_edge_cut.png)
*Pt crystal in [1-12] Zone axis orientation with edge dislocation*

## Create a crystal with twin boundary
```
#!/bin/bash

atomsk --create fcc 3.9242 Pt orient [11-2] [111] [-110] -duplicate 20 5 6 Pt_cell.xsf
atomsk Pt_cell.xsf -mirror 0 Y -wrap Pt_mirror.xsf
atomsk --merge Y 2 Pt_cell.xsf Pt_mirror.xsf Pt_twin.xsf
atomsk Pt_twin.xsf -cut below 0.3*box x -cut above 0.7*box x -cut below 0.25*box y -cut above 0.75*box y Pt_twin_cut.xyz
```
![Result](Figures/Pt_twin_cut.png)
*Pt crystal in [-110] Zone axis orientation with twin boundary*

_[1] P. Hirel, “Atomsk: A tool for manipulating and converting atomic data files,” Comput. Phys. Commun., vol. 197, pp. 212–219, Dec. 2015._

_[2] Okhotnikov, K., Charpentier, T. & Cadars, S. Supercell program: a combinatorial structure-generation approach for the local-level modeling of atomic substitutions and partial occupancies in crystals. J Cheminform 8, 17 (2016). https://doi.org/10.1186/s13321-016-0129-3_