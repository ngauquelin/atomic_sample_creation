#!/bin/bash
for f in $(ls cifs | grep .cif)
do

out_str="$(basename $f .cif)_201.xyz"
align_z cifs/"$f" [100] t1.atsk
atomsk t1.atsk -rotate y 89.7 -ow temp.atsk
# align_z input_file [zone_axis] output_file
atomsk  temp.atsk \
-orthocell  \
-duplicate 20 20 20  \
-cut above 25 X -cut below 0 X  \
-cut above 25 Y -cut below 0 Y  \
-cut above 25 Z -cut below 0 Z  \
$out_str
rm temp.atsk
rm t1.atsk

out_str="$(basename $f .cif)_011.xyz"
align_z cifs/"$f" [010] t1.atsk
atomsk t1.atsk -rotate x -134.95 -ow temp.atsk
# align_z input_file [zone_axis] output_file
atomsk  temp.atsk \
-orthocell  \
-duplicate 20 20 20  \
-cut above 25 X -cut below 0 X  \
-cut above 25 Y -cut below 0 Y  \
-cut above 25 Z -cut below 0 Z  \
$out_str
rm temp.atsk
rm t1.atsk
done